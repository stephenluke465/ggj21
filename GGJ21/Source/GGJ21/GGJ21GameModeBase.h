// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GGJ21GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GGJ21_API AGGJ21GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
